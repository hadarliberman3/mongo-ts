import { Request,Response } from "express";
import {createUserService, deleteUserService, getAllUsersService, getUserService, paginationService, updateUserService} from "../services/user.service.js";

export async function createUser(req:Request, res:Response){
    const user = await createUserService(req.body);
    res.status(200).json(user);
};

export async function getAllUsers (req:Request, res:Response){
      const users = await getAllUsersService();
      res.status(200).json(users);
}

export async function pagination(req:Request, res:Response){
    const page=Number(req.params.page);
    const count=Number(req.params.count);
    const users = await paginationService(count,page);
    res.status(200).json(users);
  };

  export async function getUser (req:Request, res:Response){
    const user = await getUserService(req.params.id,req.path);
    res.status(200).json(user);
  };

export async function updateUser(req:Request, res:Response){
    const user = await updateUserService(req.params.id,req.body);
    res.status(200).json(user);
    }

    export async function deleteUser(req:Request, res:Response){
        const user = await deleteUserService(req.params.id,req.path);
        res.status(200).json(user);
 
     }