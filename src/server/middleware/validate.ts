import { NextFunction,Request,Response } from "express";
import Joi from "joi";
import "joi-extract-type";
import { user_create_schema,user_update_schema } from "../modules/user/userjoi.schema.js";

export function isValidUserSchema(action:string){
 return async function(req:Request,res:Response,next:NextFunction){
     try{
    let value;
    if(action==="CREATE"){
      value=await user_create_schema.validateAsync(req.body);
    }
    else if(action==="UPDATE"){
      value=await user_update_schema.validateAsync(req.body);
    }
    next();
}catch(err){
    next(err);
}
 };
}