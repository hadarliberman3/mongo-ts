import mongoose from "mongoose";
const { Schema, model } = mongoose;

const UserSchema = new Schema({
    first_name  : {type : String, required:[true,"you must enter first name"]},
    last_name   : {type : String, required:[true,"you must enter last name"] },
    email       : {type : String, required: [true,"you must enter email"] },
    phone       : {type : String}
});
//         validate:{
//             validator:function(v:string){
//                 return /\d{3}-\d{3}-\d{4}/.test(v);
//             },
//             message: ()=>"is not a valid phone number"
//         },
//             required: [true,"you must enter phone"], unique: true
//         }
// },{timestamps:true});
  
export default model("user",UserSchema);