/* 
  if there is an error thrown in the DB, asyncMiddleware
  will pass it to next() and express will handle the error */
import raw from "../../middleware/route.async.wrapper.js";

import express from "express";
// import log from '@ajar/marker';
import { isValidUserSchema } from "../../middleware/validate.js";

import { createUser, deleteUser, getAllUsers, getUser, pagination,updateUser } from "../../controller/user.controller.js";

const router = express.Router();

// parse json req.body on post routes
router.use(express.json());

// CREATES A NEW USER
// router.post("/", async (req, res,next) => {
//    try{
//      const user = await user_model.create(req.body);
//      res.status(200).json(user);
//    }catch(err){
//       next(err)
//    }
// });



// CREATES A NEW USER
router.post("/",isValidUserSchema("CREATE"), raw(createUser));

// GET ALL USERS
router.get( "/",raw(getAllUsers));

//pagination
router.get( "/paginate/:page/:count",raw(pagination));

// GETS A SINGLE USER
router.get("/:id",raw(getUser));

// UPDATES A SINGLE USER
router.put("/:id",isValidUserSchema("UPDATE"), raw(updateUser));

// DELETES A USER
router.delete("/:id",raw(deleteUser));

export default router;
