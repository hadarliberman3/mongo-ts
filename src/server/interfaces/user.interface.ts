export interface Iuser{
    first_name?:string,
    last_name?:string,
    email?:string,
    phone?:string
}

export interface IresponseError{
    status:number,
    message:string,
    stack?:string
  }