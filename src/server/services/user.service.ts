import { Iuser } from "../interfaces/user.interface.js";
import user_model from "../modules/user/user.model.js";
import { URLnotFound } from "../exceptions/errors.js";



export async function createUserService(user:Iuser){
    return await user_model.create(user);
};

export async function getAllUsersService (){
    return await user_model.find()
                                  // .select(`-__v`);
                                  .select(`-_id 
                                          first_name 
                                          last_name 
                                          email 
                                          phone`);
    
};


export async function paginationService(count:number,page:number){
   return await user_model.find()
                                  // .select(`-__v`);
                                  .select(`-_id 
                                          first_name 
                                          last_name 
                                          email 
                                          phone`)
                                          .limit(count)
                                          .skip(page);
    
  };

  export async function getUserService (userId:string,path:string){
    const user = await user_model.findById(userId);
    if (!user) throw new URLnotFound(path);
    else return user;
  };

export async function updateUserService(userId:string,user:Iuser){
    return await user_model.findByIdAndUpdate(userId,user,{new: true, upsert: false });
    }

    export async function deleteUserService(userId:string, path:string){
        const user = await user_model.findByIdAndRemove(userId);
        if (!user) throw new URLnotFound(path);
        else return user;

     }